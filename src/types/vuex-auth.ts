// tslint:disable-next-line: class-name
export interface Auth_state {
	token: string;
	user_data: User_data;
}

// tslint:disable-next-line: class-name
export interface User_data {
	iat?: number;
	exp?: number;
	id?: string;
	username?: string;
}
