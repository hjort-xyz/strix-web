# Sync strategy
MSA (Manual Sync Algorithm)

## Offline scenarios
### Syncable is created or updated
Offline:
- Perfom changes
- Toggle `synced: false`
### Syncable is deleted
Offline:
- Toggle `synced: false` + `deleted: true`
- Filter `deleted: true` out when returning Syncables

### Online
- Find all `synced: false`
- Start syncing
- If `synced: false` + `deleted: true`: Delete handler
- If `synced: false` + `deleted: false`: Update handler
- If no ID: Update handler
- On positive delete repsonses, delete.
- On positive create or update repsonses, toggle: `synced: true` + overwrite with server version(`synced: true` on server?)

## Conflict scenarios
Last changes overrides. Simple. Rough.
Can cause deletes to be recreated.
This strategy should allow be open to eventual conflict handling.; 

