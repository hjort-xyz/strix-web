import { Task } from '@/types/vuex-task';

const id_filter = (id: string) => (task: Task) => task.id === id || task.temp_id === id;

export default {
	CREATE_TASK(state: any, task: Task) {
		state.push({
			...task,
			synced: false,
		});
	},
	UPDATE_TASK(state: any, incomming_task: Task) {
		// Find and extract
		const task_index = state.findIndex(id_filter(incomming_task.id || incomming_task.temp_id as string));
		let task = state[task_index];

		// Changes
		task = {
			...task,
			...incomming_task,
			synced: false,
		};

		// Save
		state.splice(task_index, 1, task);
	},
	SYNC_TASK(state: any, args: any) {
		const id: string = args.id;
		const incomming_task: Task = args.task;
		// Find and extract
		const task_index = state.findIndex(id_filter(id));
		let task = state[task_index];

		// Changes
		task = {
			...incomming_task,
			synced: true,
		};

		// Save
		state.splice(task_index, 1, task);
	},
	DELETE_TASK(state: any, id: string) {
		// Find
		const task_index = state.findIndex(id_filter(id));
		// Changes
		const task: Task = {
			...state[task_index],
			synced: false,
			deleted: true,
		};

		// Save
		state.splice(task_index, 1, task);
	},
	HARD_DELETE_TASK(state: any, id: string) {
		// Find
		const task_index = state.findIndex(id_filter(id));

		// Save
		state.splice(task_index, 1);
	},
	INIT_TASKS(state: any, tasks: Task[]) {
		tasks.map((task) => {
			task.synced = true;
			return task;
		});
		// Replace
		state.push(...tasks);
	},
	PURGE(state: any) {
		// Delete everything
		state.length = 0;
	},
};
