const msa = async (rootGetters: any, dispatch: any, vue: any) => {
	const syncable_types: string[] = [
		'task',
	];
	const actions: any[] = [];

	for (const syncable_type of syncable_types) {
		const unsynced = rootGetters[`unsynced_${syncable_type}s`];
		for (const syncable of unsynced) {
			actions.push(await sync(syncable, syncable_type, dispatch, vue));
		}
	}

	await Promise.all(actions);
};

const sync = async (syncable: any, syncable_type: string, dispatch: any, vue: any) => {
	let response;
	// Check which action to take
	switch (true) {
		case !!syncable.deleted && !syncable.temp_id:
			// Delete file on server and locally
			await vue.axios.delete(`/${syncable_type}/${syncable.id}`);
			dispatch(`hard_delete_${syncable_type}`, syncable.id);
			break;

		case !!syncable.deleted && !!syncable.temp_id:
			// Delete locally, since it never reached the server
			dispatch(`hard_delete_${syncable_type}`, syncable.id);
			break;

		case !syncable.synced && !syncable.temp_id:
			// Update on server
			response = await vue.axios.patch(`/${syncable_type}/${syncable.id}`, syncable);
			// Mark syncable as saved in state
			dispatch(`sync_${syncable_type}`, {
				[syncable_type]: response.data,
				id: syncable.id,
			});
			break;

		case !syncable.synced && !!syncable.temp_id:
			// Create on server
			response = await vue.axios.post(`/${syncable_type}`, syncable);
			// Mark syncable as saved in state
			dispatch(`sync_${syncable_type}`, {
				[syncable_type]: response.data,
				id: syncable.temp_id,
			});
			break;
	}
};

export default msa;
