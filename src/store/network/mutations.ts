import { Network_state } from '@/types/vuex-network';

export default {
	MARK_ONLINE(state: Network_state) {
		state.online = true;
	},
	MARK_OFFLINE(state: Network_state) {
		state.online = false;
	},
	START_SYNC(state: Network_state) {
		state.syncing = true;
	},
	STOP_SYNC(state: Network_state) {
		state.syncing = false;
	},
	START_WATCH(state: Network_state, interval_id: number) {
		state.interval_id = interval_id;
	},
	STOP_WATCH(state: Network_state) {
		state.interval_id = undefined;
	},
};
