export default {
	LOG_IN(state: any, context: any) {
		// Save token
		state.token = context.token;

		// Save user data
		state.user_data = context.user_data;
	},
	LOG_OUT(state: any) {
		// Remove all auth data
		state.token = null;
		state.user_data = null;
	},
};
