import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import { ModuleTree } from 'vuex';

export default {
	state: {
		token: null,
		user_data: null,
	},
	actions,
	mutations,
	getters,
} as ModuleTree<any>;
