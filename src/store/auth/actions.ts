import Vue from 'vue';
import ActionContext from '@/types/vuex-context';
import jwt from 'jsonwebtoken';
import { Auth_state } from '@/types/vuex-auth';

export default {
	log_in({ commit, dispatch }: ActionContext<Auth_state>, token: string) {
		// Decode JWT
		const user_data = jwt.decode(token);

		// Save in local store
		commit('LOG_IN', { token, user_data });
		localStorage.setItem('token', token);

		// Setup axios to use the token
		Vue.axios.defaults.headers.Authorization = `Bearer ${token}`;

		// Start sync loop
		dispatch('start_watch');
	},
	log_out({ commit, dispatch }: ActionContext<Auth_state>) {
		// Remove token and user data from store
		commit('LOG_OUT');

		// Stop watch interval
		dispatch('stop_watch');

		// Clear local data
		commit('PURGE');
		window.localStorage.clear();

		// Clear axios token
		Vue.axios.defaults.headers.Authorization = undefined;
	},
};
