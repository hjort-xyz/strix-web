import { shallowMount } from '@vue/test-utils'
import test from 'ava';
import Task from './task.vue';

test('Renders correctly without optional props', t => {
	const el = shallowMount(Task, {
		propsData: {
			title: 'A task',
		}
	});
	
	t.true(el.find('.Task').exists());
	t.true(el.find('.Task--title').exists());
	t.false(el.find('.Task--description').exists());
	t.true(el.find('.Task--toggle').exists());
});

test('Renders correctly with different props', t => {
	let propsData = {
		title: 'A task',
		description: 'And a good one at that'
	};
	const el = shallowMount(Task, { propsData });
	
	t.is(el.find('.Task--title').text(), propsData.title);
	t.is(el.find('.Task--description').text(), propsData.description);
	t.is(el.find('.Task--toggle').attributes('checked'), undefined);

	propsData = {
		title: 'Another task',
		description: 'But not a good one',
		completed: true
	};

	el.setProps(propsData);
	
	t.is(el.find('.Task--title').text(), propsData.title);
	t.is(el.find('.Task--description').text(), propsData.description);
	t.is(el.find('.Task--toggle').attributes('checked'), 'true');
});