import { shallowMount } from '@vue/test-utils'
import test from 'ava';
import Dialog from './dialog.vue';

test('Renders correctly when open', t => {
	const el = shallowMount(Dialog, {
		propsData: {
			open: true,
			title: 'A dialog',
		}
	});
	
	t.true(el.find('.Dialog--overlay').exists());
	t.true(el.find('.Dialog').exists());
	t.true(el.find('.Dialog--title').exists());
	t.true(el.find('.Dialog--content').exists());
	t.true(el.find('.Dialog--actions').exists());
});

test('Does not render when closed', t => {
	const el = shallowMount(Dialog, {
		propsData: {
			open: false,
			title: 'A dialog',
		}
	});
	
	t.false(el.find('.Dialog--overlay').exists());
	t.false(el.find('.Dialog').exists());
	t.false(el.find('.Dialog--title').exists());
	t.false(el.find('.Dialog--content').exists());
	t.false(el.find('.Dialog--actions').exists());
});
